import React, { useState, useEffect, Component } from "react";
import { Provider } from 'react-redux'
import store from './store/store'
import Header from './components/Header'
import CrawlingForm from './components/CrawlingForm'
import LogBox from './components/LogBox'
import { SocketContext, socket } from './context/socket';
import Tree from './components/Tree'
const App = () => {
  const [treeResult, setTreeResult] = React.useState({});

  return (
    <Provider store={store()}>
      <SocketContext.Provider value={socket}>
        <Header />
        <CrawlingForm />
        <LogBox />
        <Tree/>
        {/* <treeTest /> */}
      </SocketContext.Provider>
    </Provider>
  );
}

export default App;