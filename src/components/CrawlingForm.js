import React, { useState, useContext, useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { SocketContext } from '../context/socket';

const CrawlingForm = (props) => {
    let crawlDataTextBox, downloadButton
    const socket = useContext(SocketContext);

    const startCrawl = (e) => {
        e.preventDefault()
        const startUrl = document.getElementById("url")
        const maxDepth = document.getElementById("maxDepth")
        const maxPages = document.getElementById("maxPages")
        const crawlDataTextBox = document.getElementById("crawlDataText")
        const downloadButton = document.getElementById("downloadButton")
        crawlDataTextBox.value = ""
        downloadButton.disabled = true;
        socket.emit('startCrawl', { 'startUrl': startUrl.value, 'maxDepth': maxDepth.value, 'maxPages': maxPages.value })
    }

    return (
        <div className="crawlingFormWrapper">
            <div className="crawling-form">
                <form action="/crawl" method="post" id="crawlForm">
                    <h3>Start Url:</h3>
                    <input type="url" name="url" placeholder="Url" id="url" />
                    <h3>Max Depth:</h3>
                    <input type="number" name="maxDepth" placeholder="Max Depth" id="maxDepth" />
                    <h3>Max Total Pages:</h3>
                    <input type="number" name="totalPages" placeholder="Total Pages" id="maxPages" />
                    <button className="crawlButton" type="submit" onClick={startCrawl}>Crawl!</button>
                </form>
            </div>
        </div>
    )
}

const mapStateToProps = state => state

export default connect(mapStateToProps)(CrawlingForm);