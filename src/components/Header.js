import React from 'react'
import { connect } from 'react-redux';

const Header = (props) => {
    return (
        <div class="welcome-header">
            <h1>Welcome to Spider Service (aka crawler)!</h1>
        </div>
    )
}

const mapStateToProps = state => state

export default connect(mapStateToProps)(Header);