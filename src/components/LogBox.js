import React, { useState, useContext, useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { SocketContext } from '../context/socket';

const LogBox = (props) => {
    const socket = useContext(SocketContext);
    let crawlDataTextBox, downloadButton, siteId

    socket.on('msg', (msg) => {
        crawlDataTextBox.value += msg + "\n"
        crawlDataTextBox.scrollTop = crawlDataTextBox.scrollHeight;
    })

    socket.on('downloadLink', (id) => {
        downloadButton.disabled = false
        siteId.value = id
    })

    useEffect(() => {
        crawlDataTextBox = document.getElementById("crawlDataText")
        downloadButton = document.getElementById("downloadButton")
        siteId = document.getElementById("siteId")
    })



    return (
        <div className="crawlDataContainer">
            <label className="logBoxTitle">Status:</label>
            <textarea name="crawlDataText" id="crawlDataText" rows="7"></textarea>
            <form action="/file" method="post" id="downloadButtonForm">
                <input type="submit" value="Download" className="downloadBtn" id="downloadButton" disabled />
                <input type="hidden" name="id" id="siteId" value="" />
            </form>
        </div>
    )
}

const mapStateToProps = state => state

export default connect(mapStateToProps)(LogBox);