import React, { useState, useContext, useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { SocketContext } from '../context/socket';
import { Treebeard } from 'react-treebeard';

// const siteData = {
//     "request": { "maxPages": 500, "maxDepth": 50 }, "_id": "604143326d147e867888dc67", "pages":
//         [{
//             "linksArr": ["https://herocompany.azurewebsites.net/Home/About", "https://herocompany.azurewebsites.net/Home/Contact",
//                 "https://herocompany.azurewebsites.net/Account/Register", "https://herocompany.azurewebsites.net/Account/Login",
//                 "https://herocompany.azurewebsites.net/home/about", "https://herocompany.azurewebsites.net/Account/Loginasdf"],
//             "_id": "604143326d147e867888dc68", "url": "https://herocompany.azurewebsites.net/",
//             "title": "Home Page - Heroes Company", "depth": 0
//         }, {
//             "linksArr": ["https://herocompany.azurewebsites.net/Home/About",
//                 "https://herocompany.azurewebsites.net/Home/Contact", "https://herocompany.azurewebsites.net/Account/Register",
//                 "https://herocompany.azurewebsites.net/Account/Login"], "_id": "604143326d147e867888dc69", "url": "https://herocompany.azurewebsites.net/Home/About",
//             "title": "About - Heroes Company", "depth": 1
//         }, {
//             "linksArr": ["https://herocompany.azurewebsites.net/Home/About",
//                 "https://herocompany.azurewebsites.net/Home/Contact", "https://herocompany.azurewebsites.net/Account/Register", "https://herocompany.azurewebsites.net/Account/Login"], "_id": "604143326d147e867888dc6a", "url": "https://herocompany.azurewebsites.net/Home/Contact", "title": "Contact - Heroes Company", "depth": 1
//         }, {
//             "linksArr": ["http://testing3.com", "http://testing33.com"], "_id": "604143326d147e867888dc6d", "url": "https://herocompany.azurewebsites.net/Account/Loginasdf",
//             "title": "first-page-testing", "depth": 1
//         }, {
//             "linksArr": ["https://herocompany.azurewebsites.net/Home/About", "https://herocompany.azurewebsites.net/Home/Contact",
//                 "https://herocompany.azurewebsites.net/Account/Register", "https://herocompany.azurewebsites.net/Account/Login"], "_id": "604143326d147e867888dc6b",
//             "url": "https://herocompany.azurewebsites.net/Account/Register", "title": "Register - Heroes Company", "depth": 1
//         }, {
//             "linksArr":
//                 ["https://herocompany.azurewebsites.net/Home/About", "https://herocompany.azurewebsites.net/Home/Contact",
//                     "https://herocompany.azurewebsites.net/Account/Register", "https://herocompany.azurewebsites.net/Account/Login",
//                     "https://herocompany.azurewebsites.net/Account/Register"], "_id": "604143326d147e867888dc6c", "url": "https://herocompany.azurewebsites.net/Account/Login",
//             "title": " - Heroes Company", "depth": 1
//         }, {
//             "linksArr": ["https://herocompany.azurewebsites.net/Home/About",
//                 "https://herocompany.azurewebsites.net/Home/Contact", "https://herocompany.azurewebsites.net/Account/Register",
//                 "https://herocompany.azurewebsites.net/Account/Login", "https://testing2.com"], "_id": "604143326d147e867888dc6d", "url": "https://herocompany.azurewebsites.net/home/about",
//             "title": "About - Heroes Company", "depth": 1
//         }, {
//             "linksArr": ["http://testing1.com", "http://testing2.com"], "_id": "604143326d147e867888dc6d", "url": "https://testing2.com",
//             "title": "testing-testing-2", "depth": 2
//         }, {
//             "linksArr": ["http://testing3.com", "http://testing33.com"], "_id": "604143326d147e867888dc6d", "url": "http://testing1.com",
//             "title": "testing-testing-2", "depth": 3
//         }], "depth": 3, "pagesSaved": 8, "url": "https://herocompany.azurewebsites.net/",
//     "createdAt": "2021-03-04T20:29:38.370Z", "updatedAt": "2021-03-04T20:29:38.370Z", "__v": 0
// }

let tree = {}
const setRoot = (siteData) => {
    if (siteData.treeUpdateFirst)
        return {
            name: siteData.url, toggled: true, children: siteData.linksArr.map(link => {
                return { 'name': link, 'children': [], 'depth': siteData.depth }
            })
        }

    for (let i = 0; i < siteData.pages.length; i++)
        if (siteData.pages[i].depth == 0)
            return {
                name: siteData.pages[i].url, toggled: true, children: siteData.pages[/*0*/i].linksArr.map(link => {
                    return { 'name': link, 'children': [], 'depth': siteData.pages[i].depth }
                })
            }
}
const insertNodeToTree = (node, treeNode) => {
    if (node.depth - 1 === treeNode.depth && treeNode.name == node.url) {
        treeNode.children = node.linksArr.map(link => {
            return { 'name': link, 'children': [], 'depth': node.depth }
        })
        return true
    }
    else if (treeNode.children.length > 0)
        for (let j = 0; j < treeNode.children.length; j++)
            if (insertNodeToTree(node, treeNode.children[j]))
                return true
    return false
}

const makeTree = (siteData) => {
    tree = setRoot(siteData)
    for (let i = 0; i < siteData.pages.length; i++) {
        let currentNode = siteData.pages[i]
        for (let j = 0; j < tree.children.length; j++)
            insertNodeToTree(currentNode, tree.children[j])
    }
    return tree
}

const Tree = () => {
    let tree = {}//makeTree(siteData)
    const [data, setData] = useState(tree);
    const [cursor, setCursor] = useState(false);
    const socket = useContext(SocketContext);


    socket.on("treeUpdate", data => {
        // { treeDataArr, depth: curStatusParams.curDepth, parent: data.url }
        if (data.depth == 0) {
            tree = setRoot(data)
            setData(tree)
        }
        else {

        }
        //setData(makeTree(data))
    })

    socket.on("completeTree", data => {
        setData(makeTree(data))
    })

    const onToggle = (node, toggled) => {
        if (cursor) {
            cursor.active = false;
        }
        node.active = true;
        if (node.children) {
            node.toggled = toggled;
        }
        setCursor(node);
        setData(Object.assign({}, data))
    }

    return (
        <div className="treeContainer">
            <Treebeard data={data} onToggle={onToggle} />
        </div>
    )
}

const mapStateToProps = state => state
export default connect(mapStateToProps)(Tree);