import  mongoose from 'mongoose'
import  validator from 'validator'

const siteSchema = new mongoose.Schema({
    url: {
        type: String,
        required: true,
        validate: {
            validator: value => validator.isURL(value, { protocols: ['http', 'https', 'ftp'], require_tld: true, require_protocol: true }),
            message: 'Must be a Valid URL'
        }
        // trim: true
    },
    pagesSaved: {
        type: Number,
        required: true,
        validate(value) {
            if (value < 0) {
                throw new Error('Must be a postive number')
            }
        }
    },
    depth: {
        type: Number,
        required: true,
        validate(value) {
            if (value < 0) {
                throw new Error('Must be a postive number')
            }
        }
    },
    request: {
        maxPages: {
            type: Number,
            required: true,
            validate(value) {
                if (value < 0) {
                    throw new Error('Must be a postive number')
                }
            }
        },
        maxDepth: {
            type: Number,
            required: true,
            validate(value) {
                if (value < 0) {
                    throw new Error('Must be a postive number')
                }
            }
        }

    },
    pages: [{
        url: {
            type: String,
            required: true,
            validate: {
                validator: value => validator.isURL(value, { protocols: ['http', 'https', 'ftp'], require_tld: true, require_protocol: true }),
                message: 'Must be a Valid URL'
            }
        }, depth: {
            type: Number,
            required: true,
            validate(value) {
                if (value < 0) {
                    throw new Error('Must be a postive number')
                }
            }
        },
        title: {
            type: String,
            required: true
        },
        linksArr: [{
            type: String,
            required: true,
        }]
    }],
}, {
    timestamps: true
})

const Site = mongoose.model('Site', siteSchema)

// module.exports = Site
export default Site