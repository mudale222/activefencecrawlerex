import getLinksAndTitle from './fetchingData.js'
import validator from 'validator'

let linksScanned = {}
const crawlPage = (reqParams, curStatusParams, socket, users) => {
    return new Promise(async (resolve, reject) => {
        if (!users[socket.id])
            return reject("Socket not connected")

        let { startUrl, maxDepth, maxPages } = reqParams

        //first round of page crawling
        if (!curStatusParams) {
            curStatusParams = { curDepth: 0, totalPages: 0, data: [] }
            // if (curStatusParams.curDepth == 0) {
            getLinksAndTitle(startUrl, socket, users).then((res) => {
                let { title, linksArr } = res
                let depth = curStatusParams.curDepth
                let pageData = { url: startUrl, title, depth, linksArr }
                curStatusParams.data.push(pageData)
                curStatusParams.totalPages++;
                socket.emit("treeUpdate", { ...pageData, treeUpdateFirst: true })//, depth: curStatusParams.curDepth, parent: undefined })
                socket.emit("msg", "Depth: " + curStatusParams.curDepth + "/" + maxDepth + "   Pages: " + curStatusParams.totalPages + "/" + maxPages)
                curStatusParams.curDepth++;
                linksScanned[startUrl] = {}
                linksScanned[startUrl][startUrl] = true
                return resolve(crawlPage(reqParams, curStatusParams, socket, users))
            })
        }

        //all the rest of crawling rounds
        else {
            let lastDataLinks = [].concat.apply([], curStatusParams.data.filter((d) => d.depth == curStatusParams.curDepth - 1).map(d => d.linksArr)).filter(link => link !== undefined && validator.isURL(link))
            //small calc to make sure we wont get over data
            let count = lastDataLinks.length - curStatusParams.totalPages > maxPages ? maxPages - curStatusParams.totalPages : lastDataLinks.length
            if (count == 0)
                return resolve(curStatusParams.data)
            let promisesArr = [], treeDataArr = []
            for (let i = 0; i < count; i++) {
                if (!linksScanned[startUrl][lastDataLinks[i]] && !linksScanned[startUrl][lastDataLinks[i] + '/']) {
                    linksScanned[startUrl][lastDataLinks[i]] = true
                    linksScanned[startUrl][lastDataLinks[i] + '/'] = true
                    promisesArr.push(getLinksAndTitle(lastDataLinks[i], socket, users).then((data) => {
                        curStatusParams.totalPages++;
                        treeDataArr.push(data)
                        socket.emit("msg", "Depth: " + curStatusParams.curDepth + "/" + maxDepth + "   Pages: " + curStatusParams.totalPages + "/" + maxPages)
                        if (curStatusParams.totalPages % 10 == 0 || curStatusParams.totalPages - count < -10) {
                            console.log("10")
                            socket.emit("treeUpdate", { treeDataArr, depth: curStatusParams.curDepth, parent: data.url })
                            treeDataArr = []
                        }
                        // props.dispatch(updateScore(parseInt(boardAfterMove[element])))
                        return data
                    }))
                }
            }

            Promise.allSettled(promisesArr).then((res) => {
                if (!users[socket.id])
                    return reject("Socket not connected")

                res.forEach(oneResult => {
                    let { title, linksArr } = oneResult.value
                    let depth = curStatusParams.curDepth
                    let pageData = { url: oneResult.value.url /*lastDataLinks[lastDataLinks.length - 1]*/, title, depth, linksArr }
                    curStatusParams.data.push(pageData)
                })

                if (curStatusParams.curDepth > maxDepth || curStatusParams.totalPages >= maxPages) {  //max depth or max pages achived, exit
                    linksScanned[startUrl] = {}
                    return resolve(curStatusParams.data) //exit with scanned data             
                }
                else {  //all links of this level scanned, going deeper or exit if no more links
                    //Site smaller then the max page and depth, need to quit with the 'small' result
                    if (!(curStatusParams.data.some(pageData => pageData.depth == curStatusParams.curDepth && pageData.linksArr.length > 0))) {
                        socket.emit('msg', "Small site.. At least smaller then ur request parametrs... :)")
                        linksScanned[startUrl] = {}
                        // exit = true
                        return resolve(curStatusParams.data)
                    }
                    curStatusParams.curDepth++;
                    //recursive call with the next 'depth'/level data, (curStatusParams.data[curStatusParams.data.length - 1])
                    return resolve(crawlPage(reqParams, curStatusParams, socket, users))
                }
            })
        }
    }).catch(err => {
        console.log("CRAWL PAGE PROMISE ERROR:\n" + err)
    })
}

export default crawlPage