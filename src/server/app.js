import express from 'express';
const app = express();
import http from 'http'
import path from 'path'
const __dirname = path.resolve('');

app.use(express.static(path.join(__dirname, '/build')));
app.use(express.static(path.join(__dirname, '/public')))

import bodyParser from "body-parser"
app.use(bodyParser.urlencoded({
    extended: true
}));

const port = process.env.PORT || 3000
import routers from './routers.js'
app.use(routers)


app.use(function (req, res, next) {
    if ((req.path.indexOf('html') >= 0)) {
        return res.redirect('/');
    }
    next()
});

const server = http.createServer(app)
import  handleSocketConnection from './sockets.js'

let users = []

handleSocketConnection(server, users)

app.use((req, res, next) => {
    res.sendFile(path.join(__dirname, "..", "build", "index.html"));
});

server.listen(port, () => {
    console.log('server start listening on port ' + port)
})