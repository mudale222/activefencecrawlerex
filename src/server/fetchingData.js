import AbortController from 'abort-controller'
import tldjs from 'tldjs'
import fetch from 'node-fetch'
import cheerio from 'cheerio'
import validator from 'validator'
const getDomain = tldjs.getDomain
const errors = {}

const getLinksAndTitle = (url, socket, users) => {
    const controller = new AbortController();
    const timeout = setTimeout(
        () => { controller.abort(); },
        10000,
    );

    return new Promise(async (resolve, reject) => {
        let currentDomain = getDomain(url)
        await fetch(encodeURI(url), { signal: controller.signal }).then(res => res.text()).then(body => {
            const $ = cheerio.load(body);
            let linksArr = []
            let title = $("title").text() || "NOT EXIST"
            $('a').each((i, link) => {
                const href = link.attribs.href;
                if (!href)
                    ;
                else if (href.length > 1 && href[0] != '#') {
                    if (!(href.indexOf(".com") > -1) || getDomain(href) == currentDomain) {
                        if (href.startsWith('http://') || href.startsWith('https://'))
                            linksArr.push(href)
                        else if (href.startsWith('/'))
                            linksArr.push(url.startsWith("https://") ? 'https://' + currentDomain + href : 'http://' + currentDomain + href)
                        else
                            linksArr.push()
                    }
                }
            })
            linksArr = linksArr.filter(link => validator.isURL(link)) //small validation
            if (!users[socket.id].linksCounter && users[socket.id].linksCounter != 0)
                debugger;
            users[socket.id].linksCounter += linksArr.length

            return resolve({ title, linksArr, url })
        }).catch(err => {
            if (errors[url])
                errors[url]++
            else
                errors[url] = 1
            if (errors[url] > 3) {
                if (users[socket.id])
                    users[socket.id].pageErrorCounter++
                let res = { title: "NOT EXIST", linksArr: [], url }
                return resolve(res)
            }
            return resolve(getLinksAndTitle(url, socket, users))
        })
    }).catch(err => {
        users[socket.id].pageErrorCounter++
        let res = { title: "NOT EXIST", linksArr: [], url }
        return resolve(res)
    }).finally(() => {
        clearTimeout(timeout);
    });
}

// module.exports = getLinksAndTitle
export default getLinksAndTitle