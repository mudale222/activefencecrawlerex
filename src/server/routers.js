import  express from 'express'
import Site from '../database/model/site.js'
const router = new express.Router()
import tldjs from 'tldjs'
const getDomain = tldjs.getDomain
import '../database/mongoose.js'

process.on('uncaughtException', (err, origin) => {
    console.log(err)
    process.exit(1)
});

router.post('/file', async (req, res) => {
    try {
        let siteData = await Site.findOne({ _id: req.body.id })
        res.status(200)
            .attachment(getDomain(siteData.url) + ".txt")
            .send(JSON.stringify(siteData), null, 2)
    } catch (e) {
        res.status(500).send("Internal error trying download file: " + e)
    }
})

router.get("/abcd", async (req, res) => {
    return res.redirect("http://www.walla.co.il")
})

router.get('*', async (req, res) => {
    return res.redirect('index.html')
})

router.get(/html$/, async (req, res) => {
    res.redirect('/')
})

export default router