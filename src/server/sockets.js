import { Server } from 'socket.io';
import isReachable from 'is-reachable';
import validator from 'validator';
import SiteModel from '../database/model/site.js';
import crawl from './Crawl.js';

const handleSocketConnection = (server, users) => {
    const io = new Server(server    );

    try {
        console.log("Asdf")

        io.on('connection', async (socket) => {
            console.log(socket.id)

            socket.on('startCrawl', async ({ startUrl, maxDepth, maxPages }, callback) => {
                console.log("start crawl!")
                //basic validatoin
                if (!(validator.isURL(startUrl) && validator.isInt(maxDepth, { min: 1, max: 50 }) && validator.isInt(maxPages, { min: 1, max: 1000 }))) {
                    socket.emit('msg', 'Please provide correct data!\nCorrect email (start with http://)\nMax depth (1-50)\nMax pages (1-1000)\nthx')
                    return;
                }
                if (!(await isReachable(startUrl))) {
                    socket.emit('msg', 'Unfortunately the website URL that was give is unresponsive...\nPlease provide other address. thx')
                    return;
                }
                if (!users[socket.id]) {
                    users[socket.id] = { linksCounter: 0, pageErrorCounter: 0 }
                }
                else if (users[socket.id]) {
                    socket.emit('msg', 'Only 1 request per user.\nPlease wait to finish ur request.\nPaid membership can do 100 simultaneously!')
                    return;
                }

                //data already exist on database, fetch it and be done
                let cachedSites = await SiteModel.find({ url: startUrl })
                if (cachedSites && cachedSites.length > 0)
                    for (let i = 0; i < cachedSites.length; i++)
                        if (cachedSites[i].request.maxDepth > maxDepth || cachedSites[i].request.maxPages >= maxPages) {
                            socket.emit('downloadLink', cachedSites[i]._id)
                            socket.emit('msg', "DONE! U can download ur file now!")
                            socket.emit('completeTree',cachedSites[i])
                            delete users[socket.id]
                            return;
                        }

                //scanning of site that not exist on database
                socket.emit('msg', 'Crawl started' + '  Url: ' + startUrl + '  Max depth: ' + maxDepth + '  Max pages: ' + maxPages)
                crawl({ startUrl, maxDepth, maxPages }, undefined, socket, users).then(res => {
                    if (!users[socket.id])
                        return ("Socket not connected")
                    let maxDepthAchived = 0
                    res.pages = []
                    res.forEach((pageData) => {
                        maxDepthAchived = pageData.depth > maxDepthAchived ? pageData.depth : maxDepthAchived;
                        res.pages.push(pageData)
                    })
                    res.depth = maxDepthAchived
                    res.pagesSaved = res.length
                    res.url = startUrl

                    res.request = {
                        maxPages,
                        maxDepth
                    }

                    //saving new data to database and sending link to download to the user
                    socket.emit('msg', 'Collecting done.\nWe are parsing all the info now, and shortly ur data will be ready!')
                    let linkCounter = users[socket.id].linksCounter, errorCounter = users[socket.id].pageErrorCounter
                    const newSite = new SiteModel(res);
                    newSite.save(async (err) => {
                        if (err)
                            socket.emit('msg', "Unfortunately there was an error making ur file!!\n" + err)
                        else {
                            socket.emit('downloadLink', newSite.id)
                            socket.emit('msg', 'Number of links saved: ' + linkCounter + '.  Num of page error: ' + errorCounter)
                            socket.emit('msg', "DONE! U can download ur file now!")
                            socket.emit('completeTree',newSite)
                            //smaller data set, can be deleted
                            await SiteModel.deleteMany({
                                $and: [
                                    { url: res.url },
                                    { $or: [{ depth: { $lte: res.depth/*maxDepth*/ } }, { pagesSaved: { $lte: res.pagesSaved/*maxPages*/ } }] },
                                    { _id: { $ne: newSite.id } }
                                ]
                            })
                        }
                        delete users[socket.id]
                    })
                })
            })

            socket.on('disconnect', () => {
                delete users[socket.id]
            })


        })
    } catch (err) {
        console.log("!!IO ERROR!!" + err)
        debugger
    }
}

export default handleSocketConnection